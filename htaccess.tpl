#Module: ULFS
RewriteRule ^ulfs(/?)$ %sitepath%/index.php?ns=ulfs
RewriteRule ^ulfs/release/([0-9.]+)(/?)$ %sitepath%/index.php?ns=ulfs&action=release&release=$1

RewriteRule ^ulfs/release/([0-9.]+)/arch/([a-zA-z0-9.-]+)/packages(/?)$ %sitepath%/index.php?ns=ulfs&action=archpackages&release=$1&arch=$2
RewriteRule ^ulfs/release/([0-9.]+)/arch/([a-zA-z0-9.-]+)/package/([a-zA-z0-9.-]+)(/?)$ %sitepath%/index.php?ns=ulfs&action=archpackage&release=$1&arch=$2&code=$3
RewriteRule ^ulfs/release/([0-9.]+)/arch/([a-zA-z0-9.-]+)/newpackage(/?)$ %sitepath%/index.php?ns=ulfs&action=newarchpackagefrm&release=$1&arch=$2
RewriteRule ^ulfs/release/([0-9.]+)/arch/([a-zA-z0-9.-]+)/newpackage/([a-zA-z0-9.-]+)(/?)$ %sitepath%/index.php?ns=ulfs&action=newarchpackage&release=$1&arch=$2&code=$3
RewriteRule ^ulfs/release/([0-9.]+)/arch/([a-zA-z0-9.-]+)/editpackage/([a-zA-z0-9.-]+)(/?)$ %sitepath%/index.php?ns=ulfs&action=editarchpackage&release=$1&arch=$2&code=$3


RewriteRule ^ulfs/release/([0-9.]+)/arch/([a-zA-z0-9.-]+)/package/([a-zA-z0-9.-]+)/adddependances(/?)$ %sitepath%/index.php?ns=ulfs&action=addarchdependancesfrm&release=$1&arch=$2&package=$3
RewriteRule ^ulfs/release/([0-9.]+)/arch/([a-zA-z0-9.-]+)/package/([a-zA-z0-9.-]+)/editdependances(/?)$ %sitepath%/index.php?ns=ulfs&action=editarchdependancesfrm&release=$1&arch=$2&package=$3
RewriteRule ^ulfs/release/([0-9.]+)/arch/([a-zA-z0-9.-]+)/package/([a-zA-z0-9.-]+)/dependance/([a-zA-z0-9.-]+):([a-zA-z0-9.-]+)(/?)$ %sitepath%/index.php?ns=ulfs&action=editarchdependancefrm&release=$1&arch=$2&package=$3&dependance=$4&deparch=$5


RewriteRule ^ulfs/release/([0-9.]+)/packages(/?)$ %sitepath%/index.php?ns=ulfs&action=packages&release=$1
RewriteRule ^ulfs/release/([0-9.]+)/package/([a-zA-z0-9.-]+)(/?)$ %sitepath%/index.php?ns=ulfs&action=packageinfo&release=$1&package=$2
RewriteRule ^ulfs/release/([0-9.]+)/package/([a-zA-z0-9.-]+)/edit(/?)$ %sitepath%/index.php?ns=ulfs&action=packageedit&release=$1&package=$2
RewriteRule ^ulfs/release/([0-9.]+)/package/([a-zA-z0-9.-]+)/adddependances(/?)$ %sitepath%/index.php?ns=ulfs&action=adddependancesfrm&release=$1&package=$2
RewriteRule ^ulfs/release/([0-9.]+)/package/([a-zA-z0-9.-]+)/copydependances(/?)$ %sitepath%/index.php?ns=ulfs&action=copydependancesfrm&release=$1&package=$2
RewriteRule ^ulfs/release/([0-9.]+)/package/([a-zA-z0-9.-]+)/editdependances(/?)$ %sitepath%/index.php?ns=ulfs&action=editdependancesfrm&release=$1&package=$2
RewriteRule ^ulfs/release/([0-9.]+)/package/([a-zA-z0-9.-]+)/addnestings(/?)$ %sitepath%/index.php?ns=ulfs&action=addnestingsfrm&release=$1&package=$2
RewriteRule ^ulfs/release/([0-9.]+)/package/([a-zA-z0-9.-]+)/editnestings(/?)$ %sitepath%/index.php?ns=ulfs&action=editnestingsfrm&release=$1&package=$2
RewriteRule ^ulfs/release/([0-9.]+)/package/([a-zA-z0-9.-]+)/addpatch(/?)$ %sitepath%/index.php?ns=ulfs&action=addpatchfrm&release=$1&package=$2
RewriteRule ^ulfs/release/([0-9.]+)/package/([a-zA-z0-9.-]+)/editpatches(/?)$ %sitepath%/index.php?ns=ulfs&action=editpatchesfrm&release=$1&package=$2
RewriteRule ^ulfs/release/([0-9.]+)/package/([a-zA-z0-9.-]+)/patch/([a-zA-z0-9.-]+)(/?)$ %sitepath%/index.php?ns=ulfs&action=editpatchfrm&release=$1&package=$2&patch=$3
RewriteRule ^ulfs/release/([0-9.]+)/package/([a-zA-z0-9.-]+)/addaddon(/?)$ %sitepath%/index.php?ns=ulfs&action=addaddonfrm&release=$1&package=$2
RewriteRule ^ulfs/release/([0-9.]+)/package/([a-zA-z0-9.-]+)/editaddons(/?)$ %sitepath%/index.php?ns=ulfs&action=editaddonsfrm&release=$1&package=$2
RewriteRule ^ulfs/release/([0-9.]+)/package/([a-zA-z0-9.-]+)/addon/([a-zA-z0-9.-]+)(/?)$ %sitepath%/index.php?ns=ulfs&action=editaddonfrm&release=$1&package=$2&addon=$3
RewriteRule ^ulfs/release/([0-9.]+)/package/([a-zA-z0-9.-]+)/dependance/([a-zA-z0-9.-]+)(/?)$ %sitepath%/index.php?ns=ulfs&action=editdependancefrm&release=$1&package=$2&dependance=$3
RewriteRule ^ulfs/release/([0-9.]+)/newpackage(/?)$ %sitepath%/index.php?ns=ulfs&action=newpackagefrm&release=$1
RewriteRule ^ulfs/release/([0-9.]+)/patches(/?)$ %sitepath%/index.php?ns=ulfs&action=patches&release=$1
RewriteRule ^ulfs/release/([0-9.]+)/addons(/?)$ %sitepath%/index.php?ns=ulfs&action=addons&release=$1
