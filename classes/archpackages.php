<?php
class UlfsArchPackage{
var $id;
var $package;
var $arch;
//var $release;
//var $unpack;
//var $code;
//var $sourcefile;
//var $sourcedir;
var $configure;
var $build;
var $install;
//var $description;
//var $packagefile;
//var $packagefile_size;
//var $packagefile_md5;


function addDependances($deps){
global $Yaps;
//$sql[]="delete from dependances where package=$this->id";

$deps2=$Yaps->Ulfs->archpackages->itemsInCodes($this->release, $deps);
foreach($deps2 as $dep){
$sql[]="insert into architectures_dependances (package, dependance, weight) values ($this->id, $dep->id,0)";
}


$Yaps->Ulfs->db->execute($sql);
//var_dump($sql);
}

/*
function copyDependances($srcs){
global $Yaps;
//$sql[]="delete from dependances where package=$this->id";

//$srcs2=$Yaps->Ulfs->packages->itemsInCodes($this->release, $srcs);

foreach($srcs as $src){

$deps2=dependances($this->release,$src);

foreach($deps2 as $dep){
$sql[]="insert into dependances (package, dependance, weight) values ($this->id, ".$dep['id'].",".$dep['weight'].")";
}


}

$Yaps->Ulfs->db->execute($sql);
//var_dump($sql);exit;

}




*/


function removeDependances($deps){
global $Yaps;
//$sql[]="delete from dependances where package=$this->id";

$deps2=$Yaps->Ulfs->archpackages->itemsInCodes($this->release, $deps);

foreach($deps2 as $dep){
$sql[]="delete from  architectures_dependances where package=$this->id and dependance=$dep->id";
}


$Yaps->Ulfs->db->execute($sql);
var_dump($deps2,$sql);
}


/*
function addPatch($patch){
global $Yaps;
//$sql[]="delete from dependances where package=$this->id";

//$deps2=$Yaps->Ulfs->packages->itemsInCodes($this->release, $deps);
//foreach($deps2 as $dep){
$sql="insert into patches (package, filename, mode) values ($this->id, \"$patch->filename\",$patch->mode)";
//}
$Yaps->Ulfs->db->execute($sql);

var_dump($sql);
}

function getPatches(){
global $Yaps;

$sql="select id, package, filename, mode from patches where package=$this->id";
$Yaps->Ulfs->db->execute($sql);

$x=$Yaps->Ulfs->db->dataset;

$res=Array();
foreach($Yaps->Ulfs->db->dataset as $row){
$patch=new UlfsPatch();
$patch->id=$row['id'];
$patch->filename=$row['filename'];
$patch->mode=$row['mode'];
$res[]=$patch;
}
return $res;
}


function removePatches($patches){
global $Yaps;
//$sql[]="delete from dependances where package=$this->id";

//$patches2=$Yaps->Ulfs->packages->itemsInCodes($this->release, $patches);
foreach($patches as $patch){
$sql[]="delete from  patches where package=$this->id and id=$patch";
}


$Yaps->Ulfs->db->execute($sql);
//var_dump($sql);
}
*/

/*
* Add-ons
*/
/*
function addAddon($addon){
global $Yaps;
//$sql[]="delete from dependances where package=$this->id";

//$deps2=$Yaps->Ulfs->packages->itemsInCodes($this->release, $deps);
//foreach($deps2 as $dep){
$sql="insert into addons (package, filename) values ($this->id, \"$addon->filename\")";
//}
$Yaps->Ulfs->db->execute($sql);

//var_dump($sql);
}


function getAddons($addon=""){
global $Yaps;


$a="";
if($addon){
$a=" and id=$addon";
}


$sql="select id, package, filename from addons where package=$this->id $a";
//var_dump($sql);

$Yaps->Ulfs->db->execute($sql);

$x=$Yaps->Ulfs->db->dataset;

$res=Array();
foreach($Yaps->Ulfs->db->dataset as $row){
$patch=new UlfsAddon();
$patch->id=$row['id'];
$patch->filename=$row['filename'];
$res[]=$patch;
}
return $res;
}


function removeAddons($addons){
global $Yaps;
//$sql[]="delete from dependances where package=$this->id";

//$patches2=$Yaps->Ulfs->packages->itemsInCodes($this->release, $patches);
foreach($addons as $addon){
$sql[]="delete from addons where package=$this->id and id=$addon";
}


$Yaps->Ulfs->db->execute($sql);
var_dump($sql);
}



function addNestings($deps){
global $Yaps;

$deps2=$Yaps->Ulfs->packages->itemsInCodes($this->release, $deps);
foreach($deps2 as $dep){
$sql[]="insert into nestings (parent, child) values ($this->id, $dep->id)";
}


$Yaps->Ulfs->db->execute($sql);
//var_dump($sql,$Yaps->db->errors);
}

function removeNestings($deps){
global $Yaps;

$deps2=$Yaps->Ulfs->packages->itemsInCodes($this->release, $deps);
foreach($deps2 as $dep){
$sql[]="delete from nestings where parent=$this->id and child=$dep->id";
}

$Yaps->Ulfs->db->execute($sql);
//var_dump($sql);
}
*/

}

class UlfsArchPackages {
function __construct($db){
$this->db=$db;

$this->items_sql="select 
ap.id, ap.architecture, a.code arch_code,
p.id package, r.`release`, unpack, p.code, sourcefile, sourcedir, ap.configure, ap.build, ap.install,
p.description,
pf.id packagefile, pf.size packagefile_size, md5_stored packagefile_md5
from architectures_packages ap 
left join packages p on ap.package=p.id
left join releases r on p.release=r.id
left join packagesfiles_packages pf_p on pf_p.package=p.id
left join packagesfiles pf on pf.id=pf_p.packagefile
left join architectures a on a.id=ap.architecture
";

}

function mapper($dataset){
$x=array();
foreach($dataset as $v){
$obj=new UlfsArchPackage;
$obj->id=$v['id'];
$obj->package=$v['package'];
$obj->arch=$v['architecture'];
$obj->arch_code=$v['arch_code'];
$obj->release=$v['release'];
$obj->unpack=$v['unpack'];
$obj->code=$v['code'];
$obj->sourcefile=$v['sourcefile'];
$obj->sourcedir=$v['sourcedir'];
$obj->description=$v['description'];
$obj->configure=$v['configure'];
$obj->build=$v['build'];
$obj->install=$v['install'];
$obj->packagefile=$v['packagefile'];
$obj->packagefile_size=$v['packagefile_size'];
$obj->packagefile_md5=$v['packagefile_md5'];

$x[]=$obj;
}

return $x;


}


function itemsInCodes($release, $codes){

//$xcodes=array();
//foreach($codes as $code){
//$xcodes[]="\"$code\"";
//}

//$sql=$this->items_sql;
//$sql.=" where r.`release`=\"$release\" and p.code in (".strjoin($xcodes,',').")";
//$sql.=" order by p.id";
//$this->db->execute($sql);
//return $this->mapper($this->db->dataset);

$allpkgs=$this->itemsByRelease($release);

$pkgs=array();
foreach($allpkgs as $pkg){
if(in_array($pkg->code.":".$pkg->arch_code,$codes)){
	$pkgs[]=$pkg;
}

//echo "<li>".$pkg->code.":".$pkg->arch;

}


//var_dump($pkgs,$codes);exit;
return $pkgs;

}



function itemsByRelease($release,$arch=""){
$sql=$this->items_sql;
if($arch){
$sql.=" where r.`release`=\"$release\" and a.code=\"$arch\"";
}else{
$sql.=" where r.`release`=\"$release\"";
}
$sql.=" order by p.id";
$this->db->execute($sql);
return $this->mapper($this->db->dataset);
}




function itemsByReleaseAndCode($release,$arch,$code){
$sql=$this->items_sql;
$sql.=" where r.`release`=\"$release\" and a.code=\"$arch\" and p.code=\"$code\"";
$this->db->execute($sql);
return $this->mapper($this->db->dataset);
}



function items(){
/*$sql="select p.id, r.`release`, unpack, code, sourcefile, sourcedir, configure, build, install,
description,
pf.id packagefile, pf.size packagefile_size, md5_stored packagefile_md5
from packages p
left join releases r on p.release=r.id
left join packagesfiles_packages pf_p on pf_p.package=p.id
left join packagesfiles pf on pf.id=pf_p.packagefile";
////where r.`release`=\"$release\" and p.code=\"$package\"
*/
$sql=$this->items_sql." order by p.id";
//$sql="select id, `release` from releases";
$this->db->execute($sql);
/*$x=array();
foreach($this->db->dataset as $v){
$obj=new UlfsPackage;
$obj->id=$v['id'];
$obj->release=$v['release'];
$obj->unpack=$v['unpack'];
$obj->code=$v['code'];
$obj->sourcefile=$v['sourcefile'];
$obj->sourcedir=$v['sourcedir'];
$obj->configure=$v['configure'];
$obj->build=$v['build'];
$obj->install=$v['install'];
$obj->packagefile=$v['packagefile'];
$obj->packagefile_size=$v['packagefile_size'];
$obj->packagefile_md5=$v['packagefile_md5'];

$x[]=$obj;
}

return $x;
*/

return $this->mapper($this->db->dataset);

}



function add($pkg){

/*
$code=addslashes($pkg->code);
$release=addslashes($pkg->release);
$description=addslashes($pkg->description);
$sourcefile=addslashes($pkg->sourcefile);
$sourcedir=addslashes($pkg->sourcedir);
$unpack=addslashes($pkg->unpack);
$configure=addslashes($pkg->configure);
$build=addslashes($pkg->build);
$install=addslashes($pkg->install);

$sql[]="insert into packages (code, `release`, description, sourcefile, sourcedir, unpack, configure, build, install) values 
(\"$code\",$release,\"$description\",\"$sourcefile\",\"$sourcedir\", \"$unpack\", \"$configure\",\"$build\",\"$install\")";
*/
$release=addslashes($pkg->release);
$arch=addslashes($pkg->arch);
$package=addslashes($pkg->package);
$configure="#";
$build="#";
$install="#";


$sql[]="insert into architectures_packages (architecture, package, configure, build, install) values 
($arch,$package,\"$configure\",\"$build\",\"$install\")";




$sql[]="select @@identity";

$this->db->execute($sql);
//var_dump($this->db->errors);
//var_dump($sql);


}




function save($pkg){

$code=addslashes($pkg->code);
//$release=addslashes($pkg->release);
//$description=addslashes($pkg->description);
//$sourcefile=addslashes($pkg->sourcefile);
//$sourcedir=addslashes($pkg->sourcedir);
//$unpack=addslashes($pkg->unpack);
$configure=addslashes($pkg->configure);
$build=addslashes($pkg->build);
$install=addslashes($pkg->install);


/*
$sql="update architectures_packages set 
code=\"$code\", 
description=\"$description\", 
sourcefile=\"$sourcefile\", 
sourcedir=\"$sourcedir\", 
unpack=\"$unpack\", 
configure=\"$configure\", 
build=\"$build\", 
install=\"$install\" 
where id=$pkg->id";
*/
$sql="update architectures_packages set 
configure=\"$configure\", 
build=\"$build\", 
install=\"$install\" 
where id=$pkg->id";

$this->db->execute($sql);
var_dump($this->db->errors);
echo $sql;


}

}


