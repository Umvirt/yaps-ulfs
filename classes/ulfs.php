<?php

class Ulfs extends YapsModule {
function __construct($db){
$this->db=$db;
$this->releases=new UlfsReleases($db);
$this->packages=new UlfsPackages($db);
$this->architectures=new UlfsArchitectures($db);
$this->archpackages=new UlfsArchPackages($db);

}

}
