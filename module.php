<?php
include "classes/ulfs.php";
include "classes/releases.php";
include "classes/packages.php";
include "classes/architectures.php";
include "classes/archpackages.php";


include "config.php";
include "ulfs.php";
$Yaps->Ulfs=new Ulfs(new db_connection($packages_db_config));
$Yaps->addModule('Ulfs','ulfs','ULFS Package Management', 'ULFS Packages database editor');



function navmap_ulfs(){
global $Yaps;
echo "<p>";
echo "<ul>";
echo "<li><a href=".$Yaps->config['site_path']."/ulfs>ULFS</a>";
echo "<ul>";
if(@$_REQUEST['release']){
echo "<li><a href=".$Yaps->config['site_path']."/ulfs/release/".$_REQUEST['release']."/packages>Packages</a>";
echo "<ul>";
echo "<li><a href=".$Yaps->config['site_path']."/ulfs/release/".$_REQUEST['release']."/newpackage>Add package</a>";
echo "</ul>";

}
echo "</ul>";
echo "</ul>";
}
