<?php
class defaultController extends Controller{
	function defaultAct(){
//$this->yaps->Ulfs->db;
//$sql="select id, `release` from releases";
//$db->execute($sql);
//$x=$db->dataset;
//$releases=array();
$releases=$this->yaps->Ulfs->releases->items();

echo "Select release: ";
$r=Array();
foreach($releases as $v){
$r[]="<a href=".$this->yaps->config['site_path']."/ulfs/release/".$v->release."/packages>".$v->release."</a>";
}
echo join($r,' | ');

//		echo "tasty";
	}


function package2form($f,$pkg){
$f->AddField('Code','code',$pkg->code,'','Source package unique code in database');
$f->AddField('Description','description',$pkg->description,'textarea','Souce code description');
$f->AddField('Source file','sourcefile',$pkg->sourcefile,'','Souce package file');
$f->AddField('Source dir','sourcedir',$pkg->sourcedir,'','Source package folder');
$f->AddField('Unpack command','unpack',$pkg->unpack,'textarea','Command to extract source package folder from file');
$f->AddField('Configure script','configure',$pkg->configure,'textarea','Script to prepare source code for compilation');
$f->AddField('Build script','build',$pkg->build,'textarea','Script to run compilation');
$f->AddField('Install script','install',$pkg->install,'textarea','Script to install package files');
}

function archpackage2form($f,$pkg){
//$f->AddField('Code','code',$pkg->code,'','Source package unique code in database');
//$f->AddField('Description','description',$pkg->description,'textarea','Souce code description');
//$f->AddField('Source file','sourcefile',$pkg->sourcefile,'','Souce package file');
//$f->AddField('Source dir','sourcedir',$pkg->sourcedir,'','Source package folder');
//$f->AddField('Unpack command','unpack',$pkg->unpack,'textarea','Command to extract source package folder from file');
$f->AddField('Configure script','configure',$pkg->configure,'textarea','Script to prepare source code for compilation');
$f->AddField('Build script','build',$pkg->build,'textarea','Script to run compilation');
$f->AddField('Install script','install',$pkg->install,'textarea','Script to install package files');
}



function archpackagesAct(){
$release=$this->request['release'];

$arch=$this->yaps->Ulfs->architectures->getItemByCode($_REQUEST['arch']);
//var_dump($_REQUEST['arch'],$arch->id);

echo "<p>Current release: ".$release."</p>";
echo "<p>Current arch: ".$arch->code."</p>";

echo "[ <a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$arch->code."/newpackage/>New package</a> ] "; 


$x=$this->yaps->Ulfs->archpackages->itemsByRelease($release, $arch->code);
//var_dump(count($x));
if(count($x)){
foreach($x as $v){
echo "<br><a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$v->arch_code."/package/$v->code>".$v->code;

}

}


}


function newarchpackagefrmAct(){
$release=$this->request['release'];
$arch=$this->yaps->Ulfs->architectures->getItemByCode($_REQUEST['arch']);
//var_dump($_REQUEST['arch'],$arch->id);

echo "<p>Current release: ".$release."</p>";
echo "<p>Current arch: ".$arch->code."</p>";

//echo "[ <a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$arch->code."/newpackage/>New package</a> ] "; 


$x=$this->yaps->Ulfs->packages->itemsByRelease($release);
//var_dump(count($x));
if(count($x)){
foreach($x as $v){
echo "<br><a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$arch->code."/newpackage/$v->code>".$v->code."</a>";

}

}


}

function newarchpackageRawAct(){
$release=$this->request['release'];
$code=$this->request['code'];

$arch=$this->yaps->Ulfs->architectures->getItemByCode($_REQUEST['arch']);
$r=$this->yaps->Ulfs->releases->getReleaseByCode($release);

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$code);

foreach ($packages as $pkg){
$obj=new UlfsArchPackage;
$obj->release=$r->id;
$obj->arch=$arch->id;
$obj->package=$pkg->id;

$this->yaps->Ulfs->archpackages->add($obj);

}

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$arch->code."/package/".$code);


}





function archpackageAct(){
$release=$this->request['release'];
$code=$this->request['code'];
$arch=$this->yaps->Ulfs->architectures->getItemByCode($_REQUEST['arch']);
//var_dump($_REQUEST['arch'],$arch->id);

echo "<p>Current release: <a href=".$this->yaps->config['site_path']."/ulfs/release/".$release.">".$release."</a></p>";
echo "<p>Current arch: ".$arch->code."</p>";

$x=$this->yaps->Ulfs->archpackages->itemsByReleaseAndCode($release, $arch->code,$code);
//var_dump($code);
if(count($x)){
foreach($x as $v){
echo "<h1>$v->code</h1>";

echo "[ <a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$arch->code."/editpackage/$v->code>Edit</a> ]";
echo "<h2>Package info</h2>";

if($v->sourcefile){
$url=download_url($release, $v->sourcefile);

$link="<a href=$url>$url</a>";
$linkmd5="<a href=$url.md5sum>$url.md5sum</a>";

echo "Codename: ".$v->code."<br>";
echo "Source file: ".$v->sourcefile."<br>";
/*
if($v->packagefile){
echo "Source file size: ".$v->packagefile_size."<br>";
echo "Source file MD5-checkum: ".($v->packagefile_md5 ? $v->packagefile_md5 : "none") ."<br>";
}*/
echo "Source directory: ".$v->sourcedir."<br>";
//echo "Package URL: $link<br>";
//echo "Package md5-checksum URL: $linkmd5<br>";
}else{
echo "Codename: ".$v->code."<br>";

}

//echo "<br>[ <a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$arch->code."/editpackage/$v->code>Edit</a> ]<br>";


/*
$dependances=dependances($release, $v->code);

foreach($dependances as $dep){
$depends[]="<a href=".$this->yaps->config['site_path']."/ulfs/release/$release/package/".$dep['code'].">".$dep['code']."</a>";
}


if(count($dependances)){
echo "Dependances: ".strjoin($depends,", ").".<br>";
}else{
echo "Dependances: *** NO DEPENDANCES FOUND *** <br>";
}
*/

$dependances=archpkgdependances($release,  $arch->code, $v->code);

//var_dump($dependances);

foreach($dependances as $dep){
$depends[]="<a href=".$this->yaps->config['site_path']."/ulfs/release/$release/arch/".$dep['arch']."/package/".$dep['code'].">".$dep['code'].':'.$dep['arch']."</a>";
}

if(count($dependances)){
echo "Dependances: ".strjoin($depends,", ").".<br>";
}else{
echo "Dependances: *** NO DEPENDANCES FOUND *** <br>";
}


echo "<p>[ 
<a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$arch->code."/package/".$v->code."/adddependances>Add dependances</a> | 
<a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$arch->code."/package/".$v->code."/editdependances>Edit dependances</a> ]</p>";



echo "<br>";



echo "Configuration script: 
<br><pre>".configuration_script($v->configure)."</pre><br>";
echo "Build script: 
<br><pre>".build_script($v->build)."</pre><br>";
echo "Install script: 
<br><pre>".install_script($v->install)."</pre><br>";



//echo "<br><a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$v->arch_code."/package/$v->code>".$v->code;

}

}


}


function editarchpackageAct(){
$release=$this->request['release'];
$code=$this->request['code'];
$arch=$this->yaps->Ulfs->architectures->getItemByCode($_REQUEST['arch']);
//var_dump($_REQUEST['arch'],$arch->id);

echo "<p>Current release: <a href=".$this->yaps->config['site_path']."/ulfs/release/".$release.">".$release."</a></p>";
echo "<p>Current arch: ".$arch->code."</p>";

$x=$this->yaps->Ulfs->archpackages->itemsByReleaseAndCode($release, $arch->code,$code);
//var_dump($code);
if(count($x)){
foreach($x as $v){
echo "<h1>$v->code</h1>";

$f=new Formgen_Form();
$f->method='post';
$this->archpackage2form($f,$v);
$f->AddHiddenField('release',$_REQUEST['release']);
$f->AddHiddenField('package',$_REQUEST['code']);
$f->AddHiddenField('arch',$arch->code);
$f->AddHiddenField('ns','ulfs');
$f->AddHiddenField('action','savearchpackage');
$f->submitlabel="Save changes";


echo $f->render();

/*

echo "Configuration script: 
<br><pre>".configuration_script($v->configure)."</pre><br>";
echo "Build script: 
<br><pre>".build_script($v->build)."</pre><br>";
echo "Install script: 
<br><pre>".install_script($v->install)."</pre><br>";
*/

}

}


}


function savearchpackageAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$arch=$this->request['arch'];

global $Yaps;
$pkg=new UlfsPackage();

//$pkg->code=$_REQUEST['code'];
//$pkg->description=$_REQUEST['description'];
//$pkg->sourcefile=$_REQUEST['sourcefile'];
//$pkg->sourcedir=$_REQUEST['sourcedir'];
//$pkg->unpack=$_REQUEST['unpack'];
$pkg->configure=$_REQUEST['configure'];
$pkg->build=$_REQUEST['build'];
$pkg->install=$_REQUEST['install'];

$packages=$this->yaps->Ulfs->archpackages->itemsByReleaseAndCode($release,$arch,$package);
foreach($packages as $v){
$pkg->id=$v->id;
//echo 'xxx';
}

$r=$Yaps->Ulfs->releases->getReleaseByCode($_REQUEST['release']);
$pkg->release=$r->id;

$Yaps->Ulfs->archpackages->save($pkg);
//var_dump($pkg);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$arch."/package/".$package);
}



function addarchdependancesfrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$arch=$this->request['arch'];


$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){
//$pkg->id=$v->id;
echo "Package: $pkg->code<br/>";
echo "Release: $release<br/>";

$dependances=archpkgdependances($release,  $arch, $package);
//dependances($release, $package);
$depends=Array();
foreach($dependances as $dep){
$depends[]=$dep['code'].":".$dep['arch'];
}

echo "Dependances: ".strjoin($depends,',')."<br/>";
$packages2=$this->yaps->Ulfs->archpackages->itemsByRelease($release);

echo "<form action=".$this->yaps->config['site_path'].">";
foreach($packages2 as $v){
$checked="";
$pcode=$v->code;
if(in_array($v->code.":".$v->arch_code,$depends) || $v->code==$package){
$checked="checked";
$pcode="<b>$pcode</b>";
}else{
echo "<p><input id=\"dep_$v->code:$v->arch_code\" name=deps[] value=\"$v->code:$v->arch_code\" $checked type=checkbox> <label for=dep_$v->code>$pcode:$v->arch_code</label></p>";
}
}
echo "<input type=submit value=\"Add dependances\">";
echo "<input type=hidden name=release value=".$release.">";
echo "<input type=hidden name=arch value=".$arch.">";
echo "<input type=hidden name=package value=".$package.">";
echo "<input type=hidden name=ns value=ulfs>";
echo "<input type=hidden name=action value=addarchdependances>";

echo "</form>";


}
}


function addarchdependancesAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$arch=$this->request['arch'];


$deps=$this->request['deps'];


$packages=$this->yaps->Ulfs->archpackages->itemsByReleaseAndCode($release,$arch,$package);
foreach($packages as $pkg){

//var_dump($deps);

$pkg->addDependances($deps);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$arch."/package/".$pkg->code);

}


}



function editarchdependancesfrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$arch=$this->request['arch'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){
//$pkg->id=$v->id;
echo "Package: $pkg->code<br/>";
echo "Release: $release<br/>";

$dependances=archpkgdependances($release, $arch, $package);

foreach($dependances as $dep){
$depends[]="<b>".$dep['code']."</b>:".$dep['arch'];;
}

echo "Dependances: ".strjoin($depends,', ')."<br/>";


$packages2=$this->yaps->Ulfs->archpackages->itemsByRelease($release);

echo "<form action=".$this->yaps->config['site_path'].">";
foreach($dependances as $v){
$checked="";
$pcode="<a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$arch."/package/".$pkg->code."/dependance/".$v['code'].':'.$v['arch'].">".$v['code']."</a>";
echo "<p><input id=dep_".$v['code']." name=deps[] value=".$v['code'].":".$v['arch']." $checked type=checkbox> $pcode:".$v['arch']." -> ".$v['weight']."</p>";
}
echo "<input type=submit value=\"Remove dependances\">";
echo "<input type=hidden name=release value=".$release.">";
echo "<input type=hidden name=package value=".$package.">";
echo "<input type=hidden name=arch value=".$arch.">";
echo "<input type=hidden name=ns value=ulfs>";
echo "<input type=hidden name=action value=removearchdependances>";

echo "</form>";


}
}


function editarchdependancefrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$dependance=$this->request['dependance'];
$deparch=$this->request['deparch'];
$arch=$this->request['arch'];

$packages=$this->yaps->Ulfs->archpackages->itemsByReleaseAndCode($release,$arch,$package);
foreach($packages as $pkg){
//$pkg->id=$v->id;
echo "Package: $pkg->code<br/>";
echo "Release: $release<br/>";

echo "Dependance: $dependance<br/>";


//var_dump($dependance);


$dependances=archpkgdependances($release, $arch,$package,$dependance,$deparch);

foreach($dependances as $dep){
//$depends[]=$dep['code'];


//echo "Dependances: ".strjoin($depends,',')."<br/>";


$packages2=$this->yaps->Ulfs->archpackages->itemsByRelease($release);

echo "<form action=".$this->yaps->config['site_path'].">";
echo "<input name=weight value=".$dep['weight']."><br/><br/>";
echo "<input type=submit value=\"Update dependance\">";
echo "<input type=hidden name=release value=".$release.">";
echo "<input type=hidden name=package value=".$package.">";
echo "<input type=hidden name=arch value=".$arch.">";
echo "<input type=hidden name=dependance value=".$dependance.">";
echo "<input type=hidden name=deparch value=".$deparch.">";
echo "<input type=hidden name=ns value=ulfs>";
echo "<input type=hidden name=action value=savearchdependance>";

echo "</form>";
}

}
}




function savearchdependanceAct(){
$db=$this->yaps->Ulfs->db;
$release=$this->request['release'];
$package=$this->request['package'];
$dependance=$this->request['dependance'];
$arch=$this->request['arch'];
$deparch=$this->request['deparch'];


$packages=$this->yaps->Ulfs->archpackages->itemsByReleaseAndCode($release,$arch,$package);
foreach($packages as $pkg){

//var_dump($deps);

$dependances=archpkgdependances($release, $arch,$package,$dependance,$deparch);
foreach($dependances as $dep){

$sql="update architectures_dependances set weight=".$this->request['weight']." where package=".$pkg->id." and dependance=".$dep['id'];
$db->execute($sql);

var_dump($sql,$dep,$db->error);exit;
//$db->execute($sql);

}


//$pkg->removeDependances($deps);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$arch."/package/".$pkg->code);

}

}



function removearchdependancesAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$arch=$this->request['arch'];
$deps=$this->request['deps'];


$packages=$this->yaps->Ulfs->archpackages->itemsByReleaseAndCode($release,$arch,$package);
foreach($packages as $pkg){

var_dump($deps);

$pkg->removeDependances($deps);

//header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code);

}


}









function newpackagefrmAct(){
/*
echo "
<form action=".$this->yaps->config['site_path'].">
CODE: <br/><input name=code><br/>
Description:  <br/><textarea name=description></textarea><br/>
Sourcefile:  <br/><input name=sourcefile><br/>
Sourcedir:  <br/><input name=surcedir><br/>
Unpack:  <br/><textarea name=unpack></textarea></br/>
Configure:  <br/><textarea name=configure></textarea></br/>
Build:  <br/><textarea name=build></textarea></br/>
Install:  <br/><textarea name=install></textarea></br/>
<input type=submit value=\"Create package\">
<input type=hidden name=release value=".$_REQUEST['release'].">
<input type=hidden name=ns value=ulfs>
<input type=hidden name=action value=addpackage>
</form>";
*/

$pkg=new UlfsPackage();

$f=new Formgen_Form();
/*
$f->AddField('Code','code','','','Source package unique code in database');
$f->AddField('Description','description','','textarea','Souce code description');
$f->AddField('Source file','sourcefile','','','Souce package file');
$f->AddField('Source dir','sourcedir','','','Source package folder');
$f->AddField('Unpack command','unpack','','textarea','Command to extract source package folder from file');
$f->AddField('Configure script','configure','','textarea','Script to prepare source code for compilation');
$f->AddField('Build script','build','','textarea','Script to run compilation');
$f->AddField('Install script','install','','textarea','Script to install package files');
*/

$this->package2form($f,$pkg);
$f->AddHiddenField('release',$_REQUEST['release']);
$f->AddHiddenField('ns','ulfs');
$f->AddHiddenField('action','addpackage');
$f->action=$this->yaps->config['site_path'];
$f->submitlabel="Create package";


echo $f->render();
}

function addpackageAct(){
$release=$this->request['release'];

global $Yaps;
$pkg=new UlfsPackage();

$pkg->code=$_REQUEST['code'];
$pkg->description=$_REQUEST['description'];
$pkg->sourcefile=$_REQUEST['sourcefile'];
$pkg->sourcedir=$_REQUEST['sourcedir'];
$pkg->unpack=$_REQUEST['unpack'];
$pkg->configure=$_REQUEST['configure'];
$pkg->build=$_REQUEST['build'];
$pkg->install=$_REQUEST['install'];


$r=$Yaps->Ulfs->releases->getReleaseByCode($release);
$pkg->release=$r->id;

$Yaps->Ulfs->packages->add($pkg);
//var_dump($pkg);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code);


}

function packageeditAct(){
$release=$this->request['release'];
$package=$this->request['package'];

echo "<p>Current release: ".$release."</p>";

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);

foreach($packages as $v){

$f=new Formgen_Form();
$this->package2form($f,$v);
$f->AddHiddenField('release',$_REQUEST['release']);
$f->AddHiddenField('package',$_REQUEST['package']);
$f->AddHiddenField('ns','ulfs');
$f->AddHiddenField('action','savepackage');
$f->action=$this->yaps->config['site_path'];
$f->submitlabel="Save changes";


echo $f->render();



}


}

function savepackageAct(){
$release=$this->request['release'];
$package=$this->request['package'];

global $Yaps;
$pkg=new UlfsPackage();

$pkg->code=$_REQUEST['code'];
$pkg->description=$_REQUEST['description'];
$pkg->sourcefile=$_REQUEST['sourcefile'];
$pkg->sourcedir=$_REQUEST['sourcedir'];
$pkg->unpack=$_REQUEST['unpack'];
$pkg->configure=$_REQUEST['configure'];
$pkg->build=$_REQUEST['build'];
$pkg->install=$_REQUEST['install'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $v){
$pkg->id=$v->id;
//echo 'xxx';
}


$r=$Yaps->Ulfs->releases->getReleaseByCode($_REQUEST['release']);
$pkg->release=$r->id;

$Yaps->Ulfs->packages->save($pkg);
//var_dump($pkg);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code);

}

function addpatchfrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){

$f=new Formgen_Form();
$f->AddField('Filename','filename','','','Patch filename');
$f->AddField('Mode','mode','','','Patch mode');

$f->AddHiddenField('release',$_REQUEST['release']);
$f->AddHiddenField('package',$_REQUEST['package']);
$f->AddHiddenField('ns','ulfs');
$f->AddHiddenField('action','addpatch');
$f->action=$this->yaps->config['site_path'];
$f->submitlabel="Add patch";


echo $f->render();



}

}


function addpatchAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$filename=$this->request['filename'];
$mode=$this->request['mode'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){
$patch=new UlfsPatch;
$patch->filename=$filename;
$patch->mode=$mode;

$pkg->addPatch($patch);

}

//}

//header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code);


}

function editpatchesfrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];


$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){

$patches=$pkg->getPatches();

echo "<form action=".$this->yaps->config['site_path'].">";
foreach($patches as $v){
$checked="";
$pcode="<a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code."/patch/".$v->id.">".$v->filename."</a>";
echo "<p><input name=patches[] value=".$v->id." $checked type=checkbox> $pcode -> ".$v->mode."</p>";
}
echo "<input type=submit value=\"Remove patches\">";
echo "<input type=hidden name=release value=".$release.">";
echo "<input type=hidden name=package value=".$package.">";
echo "<input type=hidden name=ns value=ulfs>";
echo "<input type=hidden name=action value=removepatches>";

echo "</form>";



}

}

function removepatchesAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$patches=$this->request['patches'];


$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){

var_dump($deps);

$pkg->removePatches($patches);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code);

}


}

function editpatchfrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$patch=$this->request['patch'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){
//$pkg->id=$v->id;
echo "Package: $pkg->code<br/>";
echo "Release: $release<br/>";
echo "Patch: $patch<br/>";


//var_dump($dependance);


$patches=$pkg->getPatches($patch);

foreach($patches as $p){
//$depends[]=$dep['code'];


//echo "Dependances: ".strjoin($depends,',')."<br/>";


$packages2=$this->yaps->Ulfs->packages->itemsByRelease($release);

echo "<form action=".$this->yaps->config['site_path'].">";
echo "<input name=filename value=".$p->filename."><br/><br/>";
echo "<input name=mode value=".$p->mode."><br/><br/>";

echo "<input type=submit value=\"Update dependance\">";
echo "<input type=hidden name=release value=".$release.">";
echo "<input type=hidden name=package value=".$package.">";
echo "<input type=hidden name=patch value=".$patch.">";
echo "<input type=hidden name=ns value=ulfs>";
echo "<input type=hidden name=action value=savepatch>";

echo "</form>";
}
}
}

function savepatchAct(){
$db=$this->yaps->Ulfs->db;
$release=$this->request['release'];
$package=$this->request['package'];
$patch=$this->request['patch'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){

//var_dump($deps);

//$dependances=dependances($release, $package,$dependance);
//foreach($dependances as $dep){

$sql="update patches set filename=\"".$this->request['filename']."\", mode=".$this->request['mode']." where package=".$pkg->id." and id=".$patch;
//var_dump($sql);
$db->execute($sql);
//var_dump($db->errors);
//}


//$pkg->removeDependances($deps);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code);

}


}

/**
* Addons
*/


function addaddonfrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){

$f=new Formgen_Form();
$f->AddField('Filename','filename','','','Add-on filename');

$f->AddHiddenField('release',$_REQUEST['release']);
$f->AddHiddenField('package',$_REQUEST['package']);
$f->AddHiddenField('ns','ulfs');
$f->AddHiddenField('action','addaddon');
$f->action=$this->yaps->config['site_path'];
$f->submitlabel="Add add-on";


echo $f->render();



}

}


function addaddonAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$filename=$this->request['filename'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){
$patch=new UlfsAddon;
$patch->filename=$filename;

$pkg->addAddon($patch);

}

}


function editaddonsfrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];


$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){

$addons=$pkg->getAddons();

echo "<form action=".$this->yaps->config['site_path'].">";
foreach($addons as $v){
$checked="";
$pcode="<a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code."/addon/".$v->id.">".$v->filename."</a>";
echo "<p><input name=addons[] value=".$v->id." $checked type=checkbox> $pcode </p>";
}
echo "<input type=submit value=\"Remove add-ons\">";
echo "<input type=hidden name=release value=".$release.">";
echo "<input type=hidden name=package value=".$package.">";
echo "<input type=hidden name=ns value=ulfs>";
echo "<input type=hidden name=action value=removeaddons>";

echo "</form>";



}

}


function editaddonfrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$addon=$this->request['addon'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){
//$pkg->id=$v->id;
echo "Package: $pkg->code<br/>";
echo "Release: $release<br/>";
echo "Addon: $addon<br/>";


//var_dump($dependance);


$addons=$pkg->getAddons($addon);

foreach($addons as $a){
//$depends[]=$dep['code'];


//echo "Dependances: ".strjoin($depends,',')."<br/>";


//$addons=$this->yaps->Ulfs->packages->itemsByRelease($release);

echo "<form action=".$this->yaps->config['site_path'].">";
echo "<input name=filename value=".$a->filename."><br/><br/>";
echo "<input type=submit value=\"Update add-on\">";
echo "<input type=hidden name=release value=".$release.">";
echo "<input type=hidden name=package value=".$package.">";
echo "<input type=hidden name=addon value=".$addon.">";
echo "<input type=hidden name=ns value=ulfs>";
echo "<input type=hidden name=action value=saveaddon>";

echo "</form>";
}
}
}


function saveaddonAct(){
$db=$this->yaps->Ulfs->db;
$release=$this->request['release'];
$package=$this->request['package'];
$addon=$this->request['addon'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){

//var_dump($deps);

//$dependances=dependances($release, $package,$dependance);
//foreach($dependances as $dep){

$sql="update addons set filename=\"".$this->request['filename']."\" where package=".$pkg->id." and id=".$addon;
var_dump($sql);
$db->execute($sql);
//var_dump($db->errors);
//}


//$pkg->removeDependances($deps);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code);

}

}


function removeaddonsAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$addons=$this->request['addons'];


$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){

//var_dump($deps);

$pkg->removeAddons($addons);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code);

}
}


/*
* Dependances
*/




function adddependancesfrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){
//$pkg->id=$v->id;
echo "Package: $pkg->code<br/>";
echo "Release: $release<br/>";

$dependances=dependances($release, $package);
$depends=Array();
foreach($dependances as $dep){
$depends[]=$dep['code'];
}

echo "Dependances: ".strjoin($depends,',')."<br/>";


$packages2=$this->yaps->Ulfs->packages->itemsByRelease($release);

echo "<form action=".$this->yaps->config['site_path'].">";
foreach($packages2 as $v){
$checked="";
$pcode=$v->code;
if(in_array($v->code,$depends) || $v->code==$package){
$checked="checked";
$pcode="<b>$pcode</b>";
}else{
echo "<p><input id=\"dep_$v->code\" name=deps[] value=\"$v->code\" $checked type=checkbox> <label for=dep_$v->code>$pcode</label></p>";
}
}
echo "<input type=submit value=\"Add dependances\">";
echo "<input type=hidden name=release value=".$release.">";
echo "<input type=hidden name=package value=".$package.">";
echo "<input type=hidden name=ns value=ulfs>";
echo "<input type=hidden name=action value=adddependances>";

echo "</form>";


}
}


function adddependancesAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$deps=$this->request['deps'];


$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){

var_dump($deps);

$pkg->addDependances($deps);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code);

}


}



function copydependancesfrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){
//$pkg->id=$v->id;
echo "Package: $pkg->code<br/>";
echo "Release: $release<br/>";

$dependances=dependances($release, $package);
$depends=Array();
foreach($dependances as $dep){
$depends[]=$dep['code'];
}

$packages2=$this->yaps->Ulfs->packages->itemsByRelease($release);

echo "<form action=".$this->yaps->config['site_path'].">";
foreach($packages2 as $v){
$checked="";
$pcode=$v->code;
if(in_array($v->code,$depends) || $v->code==$package){
$checked="checked";
$pcode="<b>$pcode</b>";
}else{
echo "<p><input id=\"dep_$v->code\" name=src[] value=\"$v->code\" $checked type=checkbox> <label for=dep_$v->code>$pcode</label></p>";
}
}
echo "<input type=submit value=\"Copy dependances\">";
echo "<input type=hidden name=release value=".$release.">";
echo "<input type=hidden name=package value=".$package.">";
echo "<input type=hidden name=ns value=ulfs>";
echo "<input type=hidden name=action value=copydependances>";

echo "</form>";


}
}


function copydependancesAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$srcs=$this->request['src'];


$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){

//var_dump($srcs);

$pkg->copyDependances($srcs);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code);

}


}





function editdependancesfrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){
//$pkg->id=$v->id;
echo "Package: $pkg->code<br/>";
echo "Release: $release<br/>";

$dependances=dependances($release, $package);

foreach($dependances as $dep){
$depends[]=$dep['code'];
}

echo "Dependances: ".strjoin($depends,',')."<br/>";


$packages2=$this->yaps->Ulfs->packages->itemsByRelease($release);

echo "<form action=".$this->yaps->config['site_path'].">";
foreach($dependances as $v){
$checked="";
$pcode="<a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code."/dependance/".$v['code'].">".$v['code']."</a>";
echo "<p><input id=dep_".$v['code']." name=deps[] value=".$v['code']." $checked type=checkbox> $pcode -> ".$v['weight']."</p>";
}
echo "<input type=submit value=\"Remove dependances\">";
echo "<input type=hidden name=release value=".$release.">";
echo "<input type=hidden name=package value=".$package.">";
echo "<input type=hidden name=ns value=ulfs>";
echo "<input type=hidden name=action value=removedependances>";

echo "</form>";


}
}

function removedependancesAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$deps=$this->request['deps'];


$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){

var_dump($deps);

$pkg->removeDependances($deps);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code);

}


}


function editdependancefrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$dependance=$this->request['dependance'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){
//$pkg->id=$v->id;
echo "Package: $pkg->code<br/>";
echo "Release: $release<br/>";
echo "Dependance: $dependance<br/>";


//var_dump($dependance);


$dependances=dependances($release, $package,$dependance);

foreach($dependances as $dep){
//$depends[]=$dep['code'];


//echo "Dependances: ".strjoin($depends,',')."<br/>";


$packages2=$this->yaps->Ulfs->packages->itemsByRelease($release);

echo "<form action=".$this->yaps->config['site_path'].">";
echo "<input name=weight value=".$dep['weight']."><br/><br/>";
echo "<input type=submit value=\"Update dependance\">";
echo "<input type=hidden name=release value=".$release.">";
echo "<input type=hidden name=package value=".$package.">";
echo "<input type=hidden name=dependance value=".$dependance.">";
echo "<input type=hidden name=ns value=ulfs>";
echo "<input type=hidden name=action value=savedependance>";

echo "</form>";
}

}
}

function savedependanceAct(){
$db=$this->yaps->Ulfs->db;
$release=$this->request['release'];
$package=$this->request['package'];
$dependance=$this->request['dependance'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){

//var_dump($deps);

$dependances=dependances($release, $package,$dependance);
foreach($dependances as $dep){

$sql="update dependances set weight=".$this->request['weight']." where package=".$pkg->id." and dependance=".$dep['id'];
$db->execute($sql);

var_dump($sql,$dep,$db->error);exit;
//$db->execute($sql);

}


//$pkg->removeDependances($deps);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code);

}


}

/*
* Nestings
*/

function addnestingsfrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){
//$pkg->id=$v->id;
echo "Package: $pkg->code<br/>";
echo "Release: $release<br/>";

$dependances=nestings($release, $package);
$depends=Array();
foreach($dependances as $dep){
$depends[]=$dep['code'];
}

echo "Nestings: ".strjoin($depends,',')."<br/>";


$packages2=$this->yaps->Ulfs->packages->itemsByRelease($release);

echo "<form action=".$this->yaps->config['site_path'].">";
foreach($packages2 as $v){
$checked="";
$pcode=$v->code;
if(in_array($v->code,$depends) || $v->code==$package){
$checked="checked";
$pcode="<b>$pcode</b>";
}else{
echo "<p><input id=\"dep_$v->code\" name=nestings[] value=\"$v->code\" $checked type=checkbox> <label for=dep_$v->code>$pcode</label></p>\n";
}
}

echo "<input type=submit value=\"Add nestings\">\n";
echo "<input type=hidden name=release value=".$release.">\n";
echo "<input type=hidden name=package value=".$package.">\n";
echo "<input type=hidden name=ns value=ulfs>\n";
echo "<input type=hidden name=action value=addnestings>\n";

echo "</form>";


}
}


function addnestingsAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$nestings=$this->request['nestings'];


$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){

//var_dump($);

$pkg->addNestings($nestings);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code);

}


}


function editnestingsfrmAct(){
$release=$this->request['release'];
$package=$this->request['package'];

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){
//$pkg->id=$v->id;
echo "Package: $pkg->code<br/>";
echo "Release: $release<br/>";

$dependances=nestings($release, $package);

foreach($dependances as $dep){
$depends[]=$dep['code'];
}

echo "Nestings: ".strjoin($depends,',')."<br/>";


$packages2=$this->yaps->Ulfs->packages->itemsByRelease($release);

echo "<form action=".$this->yaps->config['site_path'].">";
foreach($dependances as $v){
$checked="";
$pcode=$v['code'];
echo "<p><input id=nesting_".$v['code']." name=nestings[] value=".$v['code']." $checked type=checkbox> $pcode </p>";
}
echo "<input type=submit value=\"Remove nestings\">";
echo "<input type=hidden name=release value=".$release.">";
echo "<input type=hidden name=package value=".$package.">";
echo "<input type=hidden name=ns value=ulfs>";
echo "<input type=hidden name=action value=removenestings>";


echo "</form>";


}
}

function removenestingsAct(){
$release=$this->request['release'];
$package=$this->request['package'];
$nestings=$this->request['nestings'];


$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);
foreach($packages as $pkg){

//var_dump($nestings);

$pkg->removeNestings($nestings);

header("Location: ".$this->yaps->config['site_path']."/ulfs/release/".$release."/package/".$pkg->code);

}


}



/*
=================================
*/


function releaseAct(){
$release=$this->request['release'];

echo "<p>Current release: ".$release."</p>";


echo "<li><a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/packages/>Packages</a>";
//echo "<li><a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/patches/>Patches</a>";
//echo "<li><a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/addons/>Addons</a>";

$x=$this->yaps->Ulfs->architectures->items();
//var_dump($x);

if(count($x)){
echo "<li>Architecture specific packages:";
echo "<ul>";
foreach ($x as $v){
echo "<li><a href=".$this->yaps->config['site_path']."/ulfs/release/".$release."/arch/".$v->code."/packages/>$v->code</a> - $v->description";
}
echo "</ul>";
}



}


function packagesAct(){
$release=$this->request['release'];

echo "<p>Current release: ".$release."</p>";

$packages=$this->yaps->Ulfs->packages->itemsByRelease($release);
$s="";
foreach($packages as $v){
$s.="<tr></td><td><a href=".$this->yaps->config['site_path']."/ulfs/release/".$v->release."/package/".$v->code.">".$v->id."</a></td><td>$v->code</td><td>$v->sourcefile</td></tr>";
}
echo "<table>$s</table>";

}

function packageinfoAct(){
$release=$this->request['release'];
$package=$this->request['package'];

echo "<p>Current release: ".$release."</p>";

$packages=$this->yaps->Ulfs->packages->itemsByReleaseAndCode($release,$package);

foreach($packages as $v){

echo "<h2>".$v->code."</h2>";

echo "<p>$v->description</p>";
echo "<p>[ <a href=".$this->yaps->config['site_path']."/ulfs/release/".$v->release."/package/".$v->code."/edit>Edit</a> ]</p>";



echo "<h3>Package info</h3>";
/*
if($v->sourcefile){
//$url=download_url($release, $v['sourcefile']);
$url="";

//$link="<a href=$url>$url</a>";
//$linkmd5="<a href=$url.md5sum>$url.md5sum</a>";

echo "Codename: ".$v->code."<br>";
echo "Source file: ".$v->sourcefile."<br>";
if($v->packagefile){
echo "Source file size: ".$v->packagefile_size."<br>";
echo "Source file MD5-checkum: ".($v->packagefile_md5 ? $v->packagefile_md5 : "none") ."<br>";
}
echo "Source directory: ".$v->sourcedir."<br>";
//echo "Package URL: $link<br>";
//echo "Package md5-checksum URL: $linkmd5<br>";
}else{
echo "Codename: ".$v->code."<br>";
}

if($v->unpack){
echo "Unpack script: 
<br><pre>".configuration_script($v->unpack)."</pre><br>";
}
echo "Configuration script: 
<br><pre>".configuration_script($v->configure)."</pre><br>";
echo "Build script: 
<br><pre>".build_script($v->build)."</pre><br>";
echo "Install script: 
<br><pre>".install_script($v->install)."</pre><br>";

*/

//echo "<li><a href=".$this->yaps->config['site_path']."/ulfs/release/".$v->release."/package/".$v->code.">".$v->code."</a>";

//echo $v->description."<br/><br/>";
//var_dump($v);



if($v->sourcefile){
$url=download_url($release, $v->sourcefile);

$link="<a href=$url>$url</a>";
$linkmd5="<a href=$url.md5sum>$url.md5sum</a>";

echo "Codename: ".$v->code."<br>";
echo "Source file: ".$v->sourcefile."<br>";
/*
if($v->packagefile){
echo "Source file size: ".$v->packagefile_size."<br>";
echo "Source file MD5-checkum: ".($v->packagefile_md5 ? $v->packagefile_md5 : "none") ."<br>";
}*/
echo "Source directory: ".$v->sourcedir."<br>";
//echo "Package URL: $link<br>";
//echo "Package md5-checksum URL: $linkmd5<br>";
}else{
echo "Codename: ".$v->code."<br>";

}

$dependances=dependances($release, $v->code);

foreach($dependances as $dep){
$depends[]="<a href=".$this->yaps->config['site_path']."/ulfs/release/$release/package/".$dep['code'].">".$dep['code']."</a>";
}


if(count($dependances)){
echo "Dependances: ".strjoin($depends,", ").".<br>";
}else{
echo "Dependances: *** NO DEPENDANCES FOUND *** <br>";
}

echo "<p>[ 
<a href=".$this->yaps->config['site_path']."/ulfs/release/".$v->release."/package/".$v->code."/adddependances>Add dependances</a> | 
<a href=".$this->yaps->config['site_path']."/ulfs/release/".$v->release."/package/".$v->code."/copydependances>Copy dependances</a>  |
<a href=".$this->yaps->config['site_path']."/ulfs/release/".$v->release."/package/".$v->code."/editdependances>Edit dependances</a> ]</p>";



$patches=patches($release,$v->code);


foreach($patches as $pat){
$url=patch_url($release,$pat['filename']);
$pats[]="<a href=\"$url\">".$pat['filename']."</a>";
}

if(count($patches)){
echo "Patches: ".strjoin($pats,", ").".<br>";
}else{
echo "Patches: *** NO PATCHES FOUND *** <br>";
}

echo "<p>[ 
<a href=".$this->yaps->config['site_path']."/ulfs/release/".$v->release."/package/".$v->code."/addpatch>Add patch</a> | 
<a href=".$this->yaps->config['site_path']."/ulfs/release/".$v->release."/package/".$v->code."/editpatches>Edit patches</a> ]</p>";


$addons=addons($release,$v->code);


foreach($addons as $addn){
$url=download_url($release,$addn);
$addns[]="<a href=\"$url\">$addn</a>";
}

if(count($addons)){
echo "Addons: ".strjoin($addns,", ").".<br>";
}else{
echo "Addons: *** NO ADDONS FOUND *** <br>";
}



echo "<p>[ 
<a href=".$this->yaps->config['site_path']."/ulfs/release/".$v->release."/package/".$v->code."/addaddon>Add add-on</a> | 
<a href=".$this->yaps->config['site_path']."/ulfs/release/".$v->release."/package/".$v->code."/editaddons>Edit add-ons</a> ]</p>";


$nestings = nestings($release,$v->code);

foreach($nestings as $nesting){
//$url=download_url($release,$addn);
//$addns[]="<a href=\"$url\">$addn</a>";
$nestings_[]="<a href=".dirname($_SERVER['SCRIPT_NAME'])."/$release/".$nesting['code'].">".$nesting['code']."</a>";
}


if(count($nestings)){
echo "Nestings: ".strjoin($nestings_,", ").".<br>";
}else{
echo "Nestings *** NO NESTINGS FOUND *** <br>";
}

echo "<p>[ 
<a href=".$this->yaps->config['site_path']."/ulfs/release/".$v->release."/package/".$v->code."/addnestings>Add nestings</a> | 
<a href=".$this->yaps->config['site_path']."/ulfs/release/".$v->release."/package/".$v->code."/editnestings>Edit nestings</a> ]</p>";



if($v->unpack){
echo "Unpack script: 
<br><pre>".configuration_script($v->unpack)."</pre><br>";
}
echo "Configuration script: 
<br><pre>".configuration_script($v->configure)."</pre><br>";
echo "Build script: 
<br><pre>".build_script($v->build)."</pre><br>";
echo "Install script: 
<br><pre>".install_script($v->install)."</pre><br>";

/*


//echo "Available packages: <table>".strjoin ($pkgs)."</table>";

$id=$v['id'];
}
*/
/*
$sql="select a.code, ap.configure, ap.build, ap.install
from packages p left join releases r on p.release=r.id 
left join packagesfiles_packages pf_p on pf_p.package=p.id 
left join packagesfiles pf on pf.id=pf_p.packagefile 
inner join architectures_packages ap on ap.package=p.id 
left join architectures a on ap.architecture=a.id
where r.`release`=\"$release\" and p.code=\"$package\"";

$db->execute($sql);
$x=array();

$x=$db->dataset;
//var_dump($sql);
if(count($x)){
echo "<h3>Arch specific instructions</h3>";
foreach($x as $v){
echo "<h4>".$v["code"]."</h4>";
echo "Configuration script: 
<br><pre>".configuration_script($v['configure'])."</pre><br>";
echo "Build script: 
<br><pre>".build_script($v['build'])."</pre><br>";
echo "Install script: 
<br><pre>".install_script($v['install'])."</pre><br>";

}
}


$x=array();
$sql="select package, text comment from comments where package=$id";
$db->execute($sql);


$x=$db->dataset;
//var_dump($sql);
if(count($x)){
echo "<hr><p>Comments:<ol>";
foreach($x as $v){
echo "<li><pre>".$v["comment"]."</pre></li>";
}
echo "</ol></p>";

}

*/





}




}




function patchesAct(){
$release=$this->request['release'];

echo $release;
}


function addonsAct(){
$release=$this->request['release'];

echo $release;
}




}
